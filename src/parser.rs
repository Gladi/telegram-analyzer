//! Этот модуль отвечает за парсинг json файла

use anyhow::Result;
use indicatif::{ProgressBar, ProgressState, ProgressStyle};
use log::info;
use std::fmt::Write;
use std::fs;
use std::io::Read;
use std::path::Path;

/// Структура DOD которая хранит результат парсинга json файла
#[derive(Debug, Clone)]
#[allow(dead_code)]
pub struct Info {
    pub name: String,
    pub r#type: String,
    pub id: u64,
    pub messages_id: Vec<u64>,
    pub messages_type: Vec<String>,
    pub messages_date: Vec<String>,
    pub messages_from: Vec<String>,
    pub messages_from_id: Vec<String>,
    pub messages_text: Vec<String>,
}

impl Info {
    /// Создание нового обьекта с capacity
    pub fn new_with_capacity(name: &String, r#type: &String, id: u64, capacity: usize) -> Self {
        Info {
            name: name.to_string(),
            r#type: r#type.to_string(),
            id,
            messages_id: Vec::with_capacity(capacity),
            messages_type: Vec::with_capacity(capacity),
            messages_date: Vec::with_capacity(capacity),
            messages_from: Vec::with_capacity(capacity),
            messages_from_id: Vec::with_capacity(capacity),
            messages_text: Vec::with_capacity(capacity),
        }
    }

    /// Выполнение reserve для всех массивов
    pub fn reserve_all(&mut self, additional: usize) {
        self.messages_id.reserve(additional);
        self.messages_type.reserve(additional);
        self.messages_date.reserve(additional);
        self.messages_from.reserve(additional);
        self.messages_from_id.reserve(additional);
        self.messages_text.reserve(additional);
    }

    /// Добавить новое сообщение
    pub fn add_message(
        &mut self,
        id: u64,
        r#type: &str,
        date: &str,
        from: &str,
        from_id: &str,
        text: &str,
    ) {
        self.messages_id.push(id);
        self.messages_type.push(r#type.to_owned());
        self.messages_date.push(date.to_owned());
        self.messages_from.push(from.to_owned());
        self.messages_from_id.push(from_id.to_owned());
        self.messages_text.push(text.to_owned());
    }
}

/// Парсинг
///
/// # Warning
/// Есть вывод в консоль!
pub fn parse(text: &str) -> Result<Info> {
    let parsed = json::parse(text)?;
    let mut info = Info::new_with_capacity(
        &parsed["name"].to_string(),
        &parsed["type"].to_string(),
        parsed["id"].as_u64().unwrap(),
        10000,
    );

    let messages = json::parse(&parsed["messages"].to_string())?;
    let mut last_number_convert_messages = 0usize;
    for (number_convert_messages, i) in (0..).enumerate() {
        if number_convert_messages == last_number_convert_messages + 10000 {
            info!("Done parsing {} messages...", number_convert_messages);
            last_number_convert_messages = number_convert_messages;
            info.reserve_all(10000);
        }

        if messages[i]["id"].as_u64().is_none() {
            break;
        }

        info.add_message(
            messages[i]["id"].as_u64().unwrap(),
            &messages[i]["type"].to_string(),
            &messages[i]["date"].to_string(),
            &messages[i]["from"].to_string(),
            &messages[i]["from_id"].to_string(),
            &messages[i]["text"].to_string(),
        );
    }

    Ok(info)
}

/// Читаем файл с ProgressBar
///
/// # Warning
/// Есть вывод в консоль!
fn read_file_with_progress_bar(path: &Path) -> Result<String> {
    const BUFFER_LEN: usize = 521;
    let mut buffer = [0u8; BUFFER_LEN];
    let mut file = fs::File::open(path)?;
    let pb = ProgressBar::new(file.metadata()?.len());
    pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {byte}/{total_bytes} ({eta})").unwrap().with_key("eta", |state: &ProgressState, w: &mut dyn Write| write!(w, "{:.1}s", state.eta().as_secs_f64()).unwrap()).progress_chars("#>-"));
    let mut result = String::new();

    loop {
        let read_count = file.read(&mut buffer)?;
        unsafe {
            result.push_str(std::str::from_utf8_unchecked(&buffer[..read_count]));
        }
        pb.inc(read_count as u64);

        if read_count != BUFFER_LEN {
            break;
        }
    }

    pb.finish_with_message("DONE READ FILE");
    Ok(result)
}

/// Парсинг файла
///
/// # Warning
/// Есть вывод в консоль!
pub fn parse_by_file(path: &Path) -> Result<Info> {
    let text = read_file_with_progress_bar(path)?;
    parse(&text)
}
