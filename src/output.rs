//! Этот модуль отвечает за вывод результатов анализаторов

use crate::analyzer::{NameAnalyzer, OutputMessage};
use anyhow::Result;
use lockfree::stack::Stack;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::PathBuf;

/// Запись в файл
pub fn write_to_file(output: &Stack<(OutputMessage, NameAnalyzer)>, path: &PathBuf) -> Result<()> {
    let mut file = OpenOptions::new().write(true).create(true).open(path)?;
    let mut to_write = Vec::new();

    for (messages, name_analyzer) in output.pop_iter() {
        writeln!(
            &mut to_write,
            "------------------NANE: {} START------------------",
            name_analyzer
        )?;

        for message in messages {
            writeln!(&mut to_write, "{}", message)?;
        }

        write!(
            &mut to_write,
            "------------------NANE: {} END------------------\n\n",
            name_analyzer
        )?;
    }

    file.write_all(&to_write[..])?;
    Ok(())
}
