use tcmalloc::TCMalloc;

#[global_allocator]
static GLOBAL: TCMalloc = TCMalloc;

mod analyzer;
mod output;
mod parser;

use log::*;
use simplelog::{ColorChoice, Config as LogConfig, LevelFilter, TermLogger, TerminalMode};
use std::path::PathBuf;
use std::sync::Arc;
use threadpool::ThreadPool;

/// Это конфиг для анализа
struct Config {
    path_to_json: PathBuf,
    value_threads: usize,
    path_to_output: PathBuf,
}

/// Получаем конфиг для анализа
fn get_config(matches: &clap::ArgMatches) -> Config {
    let matches = match matches.subcommand() {
        Some(("analyzing", matches)) => matches,
        _ => unreachable!("NOT FOUND ARGS!"),
    };

    Config {
        path_to_json: matches.get_one::<PathBuf>("json").unwrap().clone(),
        value_threads: *matches.get_one::<usize>("thread").unwrap(),
        path_to_output: matches.get_one::<PathBuf>("output").unwrap().clone(),
    }
}

/// Парсинг нашего json файла
fn parse_info(config: &Config) -> parser::Info {
    info!("PARSE FILE: {}", config.path_to_json.display());
    let info = parser::parse_by_file(&config.path_to_json).unwrap();
    info!("PARSE DONE!");

    info
}

/// Добавляем анализатор в пул потоков
macro_rules! add_analyzer {
    ($pool: ident, $path:expr) => {
        analyzer::run_analyzer_in_pool($pool, $path);
    };
}

/// Запуск всех анализаторов
fn all_run_analyzer(pool: &ThreadPool, info: &Arc<parser::Info>) {
    use analyzer::*;

    add_analyzer!(pool, most_popular_words::MostPopularWords::new(info));
    add_analyzer!(pool, count_message_for_user::CountMessageForUser::new(info));
    add_analyzer!(pool, count_words_for_user::CountWordsForUser::new(info));
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        LogConfig::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();

    let num_cpus = num_cpus::get().to_string();
    let cmd = clap::Command::new("telegram-analyzer")
        .bin_name("telegram-analyzer")
        .subcommand_required(true)
        .subcommand(
            clap::command!("analyzing").args(&[
                clap::arg!(--"json" <PATH>).value_parser(clap::value_parser!(std::path::PathBuf)),
                clap::arg!(--"thread" <NUMBER>)
                    .default_value(&num_cpus)
                    .value_parser(clap::value_parser!(usize)),
                clap::arg!(--"output" <PATH>)
                    .default_value("output.txt")
                    .value_parser(clap::value_parser!(std::path::PathBuf)),
            ]),
        );

    let matches = cmd.get_matches();
    let config = get_config(&matches);
    let info = Arc::new(parse_info(&config)); // Arc - чтобы не копировать

    info!("START ANALYZING, value threads: {}", config.value_threads);
    let pool = threadpool::ThreadPool::new(config.value_threads);
    all_run_analyzer(&pool, &info);

    pool.join();
    warn!("DONE ANALYZING {} MESSAGES!", info.messages_id.len());

    info!("START WRITE OUTPUT: {}", config.path_to_output.display());
    output::write_to_file(&analyzer::OUTPUT_MESSAGES, &config.path_to_output).unwrap();
    info!("DONE WRITE OUTPUT");
}
