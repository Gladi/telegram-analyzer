//! Анализатор
//! Количество слов у каждого пользователя

use super::{Analyzer, OutputMessage};
use crate::parser::Info;
use anyhow::Result;
use hashbrown::HashMap;
use std::sync::Arc;

type User = String;

pub struct CountWordsForUser {
    users_and_count_words: HashMap<User, usize>,
    info: Arc<Info>,
}

impl CountWordsForUser {
    pub fn new(info: &Arc<Info>) -> Self {
        CountWordsForUser {
            users_and_count_words: HashMap::new(),
            info: info.clone(),
        }
    }
}

impl Analyzer for CountWordsForUser {
    fn get_name(&self) -> &'static str {
        "Количество слов у каждого пользователя"
    }

    fn run(&mut self) -> Result<()> {
        for (message_text, message_from) in self
            .info
            .messages_text
            .iter()
            .zip(self.info.messages_from.iter())
        {
            let words = message_text.split_whitespace();
            let words_count = words.count();

            self.users_and_count_words
                .entry(message_from.to_string())
                .and_modify(|x| *x += words_count)
                .or_insert(words_count);
        }

        Ok(())
    }

    fn get_output_message(&self) -> Result<OutputMessage> {
        let mut output = OutputMessage::new();
        let mut sorted: Vec<_> = self.users_and_count_words.iter().collect();
        sorted.sort_by(|a, b| b.1.cmp(a.1));

        for i in sorted {
            output.push(format!("Пользователь: {}; Количество: {}", i.0, i.1));
        }

        Ok(output)
    }
}
