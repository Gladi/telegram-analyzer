//! Анализатор
//! Количество сообщение у каждого пользователя

use super::{Analyzer, OutputMessage};
use crate::parser::Info;
use anyhow::Result;
use hashbrown::HashMap;
use std::sync::Arc;

pub struct CountMessageForUser {
    users_and_count_message: HashMap<String, usize>,
    info: Arc<Info>,
}

impl CountMessageForUser {
    pub fn new(info: &Arc<Info>) -> Self {
        CountMessageForUser {
            users_and_count_message: HashMap::new(),
            info: info.clone(),
        }
    }
}

impl Analyzer for CountMessageForUser {
    fn get_name(&self) -> &'static str {
        "Количество сообщений у каждого пользователя"
    }

    fn run(&mut self) -> Result<()> {
        for message_from in self.info.messages_from.iter() {
            self.users_and_count_message
                .entry(message_from.to_string())
                .and_modify(|x| *x += 1)
                .or_insert(1);
        }

        Ok(())
    }

    fn get_output_message(&self) -> Result<OutputMessage> {
        let mut output = OutputMessage::new();
        let mut sorted: Vec<_> = self.users_and_count_message.iter().collect();
        sorted.sort_by(|a, b| b.1.cmp(a.1));

        for i in sorted {
            output.push(format!("Пользователь: {} Количество: {}", i.0, i.1));
        }

        Ok(output)
    }
}
