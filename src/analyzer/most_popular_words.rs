//! Анализатор
//! Самые популярные слова

const MAX_WORDS: usize = 100;

use super::{Analyzer, OutputMessage};
use crate::parser::Info;
use anyhow::Result;
use hashbrown::HashMap;
use std::sync::Arc;

pub struct MostPopularWords {
    words: HashMap<String, usize>,
    info: Arc<Info>,
}

impl MostPopularWords {
    pub fn new(info: &Arc<Info>) -> Self {
        MostPopularWords {
            words: HashMap::default(),
            info: info.clone(),
        }
    }
}

impl Analyzer for MostPopularWords {
    fn run(&mut self) -> Result<()> {
        for message_text in self.info.messages_text.iter() {
            let words = message_text.split_whitespace();

            for word in words {
                self.words
                    .entry(word.to_string().to_lowercase())
                    .and_modify(|x| *x += 1)
                    .or_insert(1);
            }
        }

        Ok(())
    }

    fn get_output_message(&self) -> Result<OutputMessage> {
        let mut sorted: Vec<_> = self.words.iter().collect();
        sorted.sort_by(|a, b| b.1.cmp(a.1));

        let mut output: OutputMessage = vec![];
        let mut number_word = 1usize;
        for i in sorted.iter() {
            output.push(format!(
                "Номер: {}; Сообщение: {}; Количество: {}",
                number_word, i.0, i.1
            ));

            if number_word == MAX_WORDS {
                break;
            }
            number_word += 1;
        }

        Ok(output)
    }

    fn get_name(&self) -> &'static str {
        "Анализатор самого часто используемого слова"
    }
}
