//! Этот млдуль описывает trait для анализатора

pub mod count_message_for_user;
pub mod count_words_for_user;
pub mod most_popular_words;

use anyhow::Result;
use lazy_static::lazy_static;
use lockfree::stack::Stack;
use log::*;
use threadpool::ThreadPool;

pub type NameAnalyzer = &'static str;
pub type OutputMessage = Vec<String>;
lazy_static! {
    /// Тут храняться все сообщение для вывода
    pub static ref OUTPUT_MESSAGES: Stack<(OutputMessage, NameAnalyzer)> = Stack::new();
}

/// Сам главный trait
pub trait Analyzer {
    fn run(&mut self) -> Result<()>;
    fn get_output_message(&self) -> Result<OutputMessage>;
    fn get_name(&self) -> &'static str;
}

/// Запуск анализатора в пул потоковы
pub fn run_analyzer_in_pool(pool: &ThreadPool, mut analyzer: impl Analyzer + Send + 'static) {
    pool.execute(move || {
        info!("START ANALYZER: {}", analyzer.get_name());

        if analyzer.run().is_err() {
            error!("ERROR RUN ANALYZER: {}", analyzer.get_name());
        }

        if let Ok(output) = analyzer.get_output_message() {
            info!("GET OUTPUT MESSAGE ANALYZER: {}", analyzer.get_name());
            OUTPUT_MESSAGES.push((output, analyzer.get_name()));
        } else {
            error!("ERROR GET OutputMessage: {}", analyzer.get_name());
        }

        info!("DONE ANALYZER: {}", analyzer.get_name());
    });
}
